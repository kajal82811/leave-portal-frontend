import React, { useState,useEffect } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { About } from './component/About';
import { Layout } from './component/Layout';
import { NavigationBar } from './component/NavigationBar';
import LeaveRequestForm from './component/LeaveRequestForm';
import LeaveTable from './component/LeaveTable';
import { Button, Alert, Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './component/LoginPage';
import SignUp from './component/SignUp';
import Admin from './component/Admin';
import { Provider } from 'react-redux';
import TotalLeave from "./component/TotalLeave.js"


const checkToken = () => {
  if(localStorage.getItem('userInfo')){
    return  <NavigationBar />
  }else{
    return null
  }

}


const App = () => {
  const [token, setToken] = useState();
 useEffect(() => {
  //  localStorage.clear()
 
 });
  return (
    
    
       <React.Fragment>
          {/* <NavigationBar /> */}
          {checkToken()}
          
       
            <Router>
           
              <Switch>
                
                
            
                <Route exact path="/" component={Login} />
                <Route exact path="/signup" component={SignUp} />
                {/* <Route path="/about" component={About} /> */}
                {/* <Route exact path="/leave-form" component={LeaveRequestForm} /> */}
             
                <Route  exact path="/request-form" component={LeaveRequestForm} />
                {/* <Route exact path="/leave" component={LeaveTable} /> */}
               
              
                {/* <Route exact path="/leadstatus" component={Admin} /> */}
                {/* <Route exact path="/home" component={Home} /> */}
                <Route exact path="/leave" component={LeaveTable} />
                <Route exact path="/admin" component={Admin} />
                <Route exact path="/leaveleft" component={TotalLeave} />
                {/* <Route exact path="/home" component={Home} /> */}
                
           
              </Switch>
            </Router>
         
  
        </React.Fragment>
      
  
  );
}

export default App;




