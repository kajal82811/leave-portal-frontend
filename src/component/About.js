
import React from 'react';


export const About = () => (
    <div>
        <h1>About Page</h1>
        <p>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.</p>
        <p>Build encapsulated components that manage their own state, then compose them to make complex UIs.</p>

    </div>
)
