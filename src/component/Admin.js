import React, { Component } from 'react';
// import Home from "./Home"
import {Link} from "react-router-dom";
import BootstrapTable from 'react-bootstrap-table-next';
// import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, {CSVExport, Search} from "react-bootstrap-table2-toolkit";
import Chips from 'react-chips';
import paginationFactory from "react-bootstrap-table2-paginator";
// import filterFactory from "react-bootstrap-table2-filter";
import axios from 'axios';
import CheckboxGroup from 'react-checkbox-group';
import { Form, Button, Col,Badge,Container} from 'react-bootstrap';
import ReactLabel from 'react-label';
import { NavigationBar } from './NavigationBar';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckSquare, faTimes } from '@fortawesome/fontawesome-free-solid'
import filterFactory, { selectFilter,textFilter } from 'react-bootstrap-table2-filter';
import "./LeaveTable.css"

let qualityFilter;
const selectOptions = {
  'Accepted': 'Accepted',
  'Rejected': 'Rejected',
  'Pending': 'Pending'
};

function rankFormatter(cell, row, rowIndex, formatExtraData) {
  if(row.status=="Accepted"||row.status=="accepted"){
    return (
      < div
          style={{ textAlign: "center",
             cursor: "pointer",
            lineHeight: "normal" }}>
              
              
<Badge style={{ padding: "5px",
    marginTop: "12px",
    height: "20px"
}} variant="success">{row.status}</Badge>
 </div>)
  } else if(row.status=="Rejected"||row.status=="rejected"){
    return (
      < div
          style={{ textAlign: "center",
             cursor: "pointer",
            lineHeight: "normal" }}>
              
              
<Badge style={{ padding: "5px",
    marginTop: "12px",
    height: "20px"
}} variant="danger">{row.status}</Badge>
 </div>)
  } else if(row.status=="Pending"||row.status=="pending"){
    return (
      < div
          style={{ textAlign: "center",
             cursor: "pointer",
            lineHeight: "normal" }}>
              
              
<Badge style={{ padding: "5px",
    marginTop: "12px",
    height: "20px"
}}  variant="warning">{row.status}</Badge>
 </div>)
  }else if(row.status=="Cancel"||row.status=="cancel"){
    return (
      < div
          style={{ textAlign: "center",
             cursor: "pointer",
            lineHeight: "normal" }}>
              
              
<Badge style={{ padding: "5px",
    marginTop: "12px",
    height: "20px"
}}  variant="secondary">{row.status}</Badge>
 </div>)

  }else{
    return (
      < div
          style={{ textAlign: "center",
             cursor: "pointer",
            lineHeight: "normal" }}>
              
              
<Badge  style={{ padding: "5px",
    marginTop: "12px",
    height: "20px"
}}  variant="danger">{row.status}</Badge>
 </div>)

  }
   }

const acceptrequest = (row) => {
  console.log(row)
  const x = {
    id:row._id,
   status:"Accepted",
    
  }
  

      axios.post(`/leave/status`, {x},{headers:{"authorization": localStorage.getItem("userInfo")}})
      .then(res => {
        
        console.log(res);
        console.log(res.data);
        console.log("do it");
        
        // window.location.href = "./leave"
      }).catch(error => {
       console.log(error)
      });
    
    }

    const rejectrequest = (row) => {
      console.log(row)
      const x = {
        id:row._id,
       status:"Rejected"
      }
      

          axios.post(`/leave/status`, {x},{headers:{"authorization": localStorage.getItem("userInfo")}})
      .then(res => {
        
        console.log(res);
        console.log(res.data);
        console.log("do it");
        
        
      }).catch(error => {
       console.log(error)
      });
        
        }

        const cancelrequest = (row) => {
          console.log(row)
          const x = {
            id:row._id,
           status:"cancel"
          }
          
    
              axios.post(`/leave/status`, {x},{headers:{"authorization": localStorage.getItem("userInfo")}})
          .then(res => {
            
            console.log(res);
            console.log(res.data);
            console.log("do it");
            
            
          }).catch(error => {
           console.log(error)
          });
            
            }


            const deleterequest = (row) => {
              console.log(row)
              const x = {
                id:row._id,
              
              }
              
        
                  axios.post(`/leave/delete`, {x},{headers:{"authorization": localStorage.getItem("userInfo")}})
              .then(res => {
                
                console.log(res);
                console.log(res.data);
                console.log("do it");
                
                
              }).catch(error => {
               console.log(error)
              });
                
                }
    





        const {ExportCSVButton} = CSVExport;
        const {SearchBar, ClearSearchButton} = Search;
const columns = [{
  dataField: 'user.name',
  text: 'Name',
  sort: true,
//   formatter: cell => selectOptions[cell],
// filter: selectFilter({
//   options: selectOptions
// })
filter: textFilter()

},{
    dataField: 'start_date',
    text: 'Start Date',
    formatter: function dateFormatter(cell, row) {
      return (
          <span><div>{moment(cell).format('DD/MM/YYYY')}</div></span>
      )},
    sort: true,
  
  }, {
    dataField: 'end_date',
    text: 'End Date',
    formatter: function dateFormatter(cell, row) {
      return (
          <span><div>{moment(cell).format('DD/MM/YYYY')}</div></span>
      )},
    sort: true,
    
  },
    {
    dataField: 'msg',
    text: 'Reason',
    sort: true,
  },
  {
    dataField: 'duration',
    text: 'Duration',
    sort: true,
  },
  {
    dataField: 'type',
    text: 'Type',
    sort: true,
  //   formatter: cell => selectOptions[cell],
  // filter: selectFilter({
  //   options: selectOptions
  // })
  filter: textFilter()
  
  },
  {
    dataField: 'created_date',
    text: 'Requested',
    sort: true,
    formatter: function dateFormatter(cell, row) {
      return (
          <span><div>{moment(cell).format('MMM DD, YYYY')}</div></span>
      )}
  },{
    dataField: 'status',
    text: 'Status',
    sort: true,
    formatter: rankFormatter,
    // filter: textFilter()
    filter: selectFilter({
      options: selectOptions,
      getFilter: (filter) => {
          // qualityFilter was assigned once the component has been mounted.
          qualityFilter = filter;
      }
  })
  },{
    dataField: '_id',
    text: 'Action',
    sort: true,
    formatter: (cell, row) => <div style={{display:"flex",justifyContent:"space-evenly"}}><div onClick={()=>acceptrequest(row)}><FontAwesomeIcon style={{color:"green",fontSize:"40px",cursor: "grab"}} icon={faCheckSquare} /></div> 
  
    <div onClick={()=>rejectrequest(row)}><FontAwesomeIcon style={{color:"red",fontSize:"40px",cursor: "grab"}} icon={faTimes} /></div>
   </div>,
   
  }];





class LeaveTable extends Component {
  constructor(props){
    super(props)
      this.state ={
        result:[]


    }
  }
  handleGetCurrentData = () => {
    console.log("nnn", this.node.table.props.data);
}






    componentDidMount(){



        // console.log("in detail",id)
      axios.post(`/leave/list`,{
            id: "602e139d5c3ead4657912075",
      }
        )
      .then(res => {
        console.log(res);
        console.log(res.data.user)
    //     const listItems = res.data.user.map.map((item,key) =>
    // // Wrong! The key should have been specified here:
    //       console.log("item",item._id)
    //  );
       
        this.setState({result:res.data.user})
        console.log(this.state.result)


      })



    }
    render() {
        return (
          <Container fluid>
           <div>
              <h2>Admin requests</h2>
              <div style={{display:"flex",justifyContent: "space-between"}}>
   {/* <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label column sm="8">Leave Type</Form.Label>
                    <Col sm="12">
                    <Form.Control as="select">
                        <option>Compensate</option>
                        <option>Sick leave</option>
                        <option>Maternity Leave</option>
                        <option>Paternity Leave</option>
                        <option>Paid Leave</option>
                        <option>Special Leave</option>
                    </Form.Control>
                    </Col>
                </Form.Group> */}
                {/* <div>
                <Badge variant="secondary"><input type="checkbox"/>Planned</Badge>{' '}
                <Badge variant="success"><input type="checkbox"/>Accepted</Badge>{' '}
                <Badge variant="warning"><input type="checkbox"/>Requested</Badge>{' '}
                <Badge variant="danger"><input type="checkbox"/>Rejected</Badge>{' '}
                <Badge variant="danger"><input type="checkbox"/>Cancellation</Badge>{' '}
                <Badge variant="danger"><input type="checkbox"/>Canceled</Badge>{' '}
                </div> */}
                </div>
          <div>
            <ToolkitProvider

              keyField="_id"
              data={this.state.result}
              columns={columns}
              exportCSV={{onlyExportFiltered: true, exportAll: false}}
              search
              >
                
              {(props) => (
              


        <div>
          {/* <div style={{float:"right",marginTop:"10px",marginBottom:"20px"}}>
          <SearchBar {...props.searchProps} onChange={this.handleGetCurrentData}/>
            <ClearSearchButton {...props.searchProps}   />
            <ExportCSVButton {...props.csvProps} style={{
                                      border: "2px solid #007bff",
                                      background: "#007bff",
                                    color: "white",
                                    textDecoration: "none",

                                }}>Export this List</ExportCSVButton>
            </div> */}

              <BootstrapTable
               {...props.baseProps}
              bordered
              hover
              striped
              columns={ columns }
              keyField="id"
              // filter={filterFactory()}
              data={this.state.result}
              pagination={ paginationFactory() }

              filter={ filterFactory() }

              wrapperClasses="table-responsive"
             
              />

            </div>

              )}




              </ToolkitProvider>
           </div>


        </div>
        </Container>
        )

    }
}

export default LeaveTable;
