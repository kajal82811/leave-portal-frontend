import React, { useEffect, useRef, useState } from 'react';


// import DatePicker from "react-bootstrap-date-picker";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Nav, Navbar,Container } from 'react-bootstrap';
import { Form, Button, Col } from 'react-bootstrap';
import styled from 'styled-components';
import axios from 'axios';
// import moment from 'moment';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { Link } from 'react-router-dom';
import LeaveTable from './LeaveTable';
import lodash from 'lodash'
var moment = require('moment-business-days');


let duration2020 = []
const options = [
    'compensate', 'sick leave', 'maternity leave','paternity leave',
    'paid leave','special'
  ];
const holidaylist = [
   
  "01/01/2021",
     
  
    "01/14/2021",
     
    "01/20/2021",
   
    "01/26/2021",
    
   
   
  
   
      "03/11/2021",
    
    "03/28/2021",
    
     "03/29/2021",
    
  
   "04/02/2021",
    
   
   
    "04/14/2021",
    
     "04/14/2021",
   
 
     "04/21/2021",
     
  
   
    
   "05/13/2021",
    
    "05/26/2021",
      
   
   
   "07/20/2021",
      
     "07/24/2021",
     
  "08/15/2021",
    
  

    
   "08/22/2021",
    
     "08/30/2021",
    
    "08/30/2021",
    
    "09/10/2021",
      
 
  
      "10/02/2021",
  

       "10/14/2021",
      
   "10/15/2021",
     
   
       "10/20/2021",
 
     
   
   
       "11/04/2021",
    
   "11/04/2021",
     
    "11/05/2021",
    
   
      "11/06/2021",
      
     "11/10/2021",
      
      
      "11/19/2021",
     
 
      "11/24/2021",
      
   
   
   
     "12/24/2021",
    
   
     "12/25/2021",
     
    
   
       "12/31/2021",
      
    
  ];

export const LeaveRequestForm = () => {
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState(new Date())
    const [value, setValue] = useState();
    const [leavetype,setLeavetype] = useState("");
    const [textArea,settextArea] = useState("");
    const [validated, setValidated] = useState(false);
    const [result, setResult] = useState([]);

    let date = [];
    let startdate = startDate;
    let enddate = endDate;
    while (moment(startdate, "MM/DD/YYYY").valueOf() <= moment(enddate, "DD/MM/YYYY").valueOf()) {
        if ((moment(startdate).day() != "0" && moment(startdate).day() != "6")) {
            date.push(moment(startdate, "MM/DD/YYYY").format("MM/DD/YYYY"));
        }
        // totalDates.push(moment(startdate, "MM/DD/YYYY").format("MM/DD/YYYY"));
        startdate = moment(startdate, "MM/DD/YYYY").add(1, "days").format("MM/DD/YYYY");
    }
    console.log(date);

    const newArray = lodash.intersection(date, holidaylist)
    console.log("intersection", newArray)
    console.log("intersection", newArray.length)

    var diff = date.length - newArray.length;
   

// let startdate = startDate;
// let enddate = endDate;
// while (moment(startdate, "MM/DD/YYYY").valueOf() <= moment(enddate, "DD/MM/YYYY").valueOf()) {
//   date.push(moment(startdate, "MM/DD/YYYY").format("MM/DD/YYYY"));
//   startdate = moment(startdate, "MM/DD/YYYY").add(1, "days").format("MM/DD/YYYY");
// }
// console.log(date);

// const newArray = lodash.intersection(date,holidaylist)
// console.log("intersection",newArray)
// console.log("intersection",newArray.length)
// var haha = []
// console.log(haha)

// var diff = moment(startDate, 'MM-DD-YYYY').businessDiff(moment(endDate,'MM-DD-YYYY'));


// if (moment(startDate, "MM/DD/YYYY").isSame(endDate, 'date')) {
 
//     diff = 1;
//   }if (diff > 1){
//     diff=diff+1
    
//    newArray.map((item) => {
//       if((moment(item).day()=="0" || moment(item).day()=="6")){
//          console.log(item)
//          const c = [item]
//           console.log(c)
//           haha.push(c)
//           console.log(haha)
//        diff = diff-(newArray.length)+(haha.length)
//       // diff=diff-2
      
//      }
//    })

//   }
// diff= diff+1
// console.log(diff)

//  newArray.map((item) => {
//    if(moment(item).day()=="0" || moment(item).day()=="6"){
//     console.log(moment(item).day())
//     diff = diff-newArray.length
//   }
// })



//   let sd = moment(startdate, "MM/DD/YYYY").valueOf();
//   let ed = moment(endDate, "DD/MM/YYYY").valueOf();
// if (moment(startdate, "MM/DD/YYYY").isSame(endDate, 'date')) {
//   diff = 1;
// }
// duration = diff;
//     if (diff > 1) {
//         for (let holiday of holidaylist) {
//             let hd = moment(holiday.date, "MM/DD/YYYY").valueOf()
//             if (sd <= hd && hd <= ed) {
//               if (holiday.week_day !== 'Saturday' || holiday.week_day !== 'Sunday') {
//                   duration--;
//               }
//             }
//         }
//     }

const hihih = () => {
    axios.post(`/leave/list`,{
        id: "602e139d5c3ead4657912075",
    }
    )
    .then(res => {
    console.log(res);
    console.log(res.data.user)
    //     const listItems = res.data.user.map.map((item,key) =>
    // // Wrong! The key should have been specified here:
    //       console.log("item",item._id)
    //  );
    
    setResult(res.data.user)
    console.log(result)
    var ll = moment(startDate).format("YYYY/MM/DD")
    console.log(ll)

var duration2021 = []
var duration2122 = []
if(moment(ll).isBetween("2020/03/31", "2021/04/01")){
 result.map(element=>duration2021.push(element.duration))
}
if(moment(ll).isBetween("2021/03/31", "2022/04/01")){
 result.map(element=>duration2122.push(element.duration))
}
console.log(duration2021)
console.log(duration2122)




})



}

    const handleChange = (event) =>{
        console.log(event.target.value)
        settextArea(event.target.value)
    }

    let Selectedleave;
    const handleSelect = (option) =>{
     Selectedleave = option.value;
    console.log(Selectedleave)
    setLeavetype(option.value)
    console.log(leavetype)
    // setLeavetype(Selectedleave)

}


    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(`Submitting Name ${value}`);
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
          e.preventDefault();
          e.stopPropagation();
        }

    setValidated(true);
        const details = {
            user: "602e139d5c3ead4657912075",
            start_date: moment(startDate).format('YYYY-MM-DD'),
            end_date: moment(endDate).format('YYYY-MM-DD'),
            duration: diff,
            type: leavetype,
            msg: textArea,
        };
        console.log("in detail",details)

    //   axios.post(`/leave/add`, { details},{headers:{"authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAcHMuY29tIiwiaWF0IjoxNjE0NjAwMDkyLCJleHAiOjE2MTQ2ODY0OTJ9.bOz77esHPw_Pn15CVVbisnW_MDr4UX09aP3fS0gc7Mc"}})
    //   .then(res => {
    //     console.log(res);
    //     console.log(res.data);
    //     console.log("do it");
    //   })

      axios.post(`/leave/add`, { details},{headers:{"authorization": localStorage.getItem("userInfo")}})
      .then(res => {

        console.log(res);
        console.log(res.data);
        console.log("do it");

        window.location.href = "./leave"
      }).catch(error => {
       console.log(error)
      });
    };






    return (
        <Container  style={{marginTop:"50px"}}>
           
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group controlId="exampleForm.ControlSelect1 validationCustom01">
                    <h2>Submit a leave request</h2>
                    
                    <Form.Label>Leave Type</Form.Label>
                    <Dropdown options={options} value={leavetype} onChange={handleSelect}  />
                </Form.Group>

                <Form.Row>
                    <Col>
                        <Form.Group controlId="exampleForm.ControlInput2 validationCustom02">
                           
                            <Form.Label>Start Date</Form.Label>
                            <div className="form-group">
                                <DatePicker
                                    selected={startDate}
                                    onChange={date => setStartDate(date)}
                                    selectsStart
                                    startDate={startDate}
                                    endDate={endDate}
                                />
                            </div>
                           

                        </Form.Group>
                    </Col>
                    {/* <Col>
                        <Form.Group controlId="exampleForm.ControlSelect2">
                            <Form.Label></Form.Label>
                            <Form.Control as="select">
                                <option>Morning</option>
                                <option>Afternoon</option>
                            </Form.Control>
                        </Form.Group>
                    </Col> */}
                </Form.Row>

                <Form.Row>
                    <Col>
                        <Form.Group controlId="exampleForm.ControlInput3">
                            <Form.Label>End Date</Form.Label>
                            <div className="form-group">
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                />
                            </div>
                        </Form.Group>
                    </Col>

                    {/* <Col>
                        <Form.Group controlId="exampleForm.ControlSelect3">
                            <Form.Label></Form.Label>
                            <Form.Control as="select">
                                <option>Afternoon</option>
                                <option>Morning</option>
                                <option>Full day</option>
                            </Form.Control>
                        </Form.Group>
                    </Col> */}
                </Form.Row>

                {/* <button onClick={changeDuration}>Click</button> */}

                <Form.Row>
                    <Form.Label>Duration</Form.Label>
                    <Form.Control
                        type="text"
                        // value={duration}
                        value={diff}
                        onChange={(e) => setValue(e.target.value)}
                        required
                    />

                </Form.Row>

                <Form.Group controlId="exampleForm.ControlTextarea1 validationCustomUsername">
                    <Form.Label style={{marginTop: "20px"}}>Cause</Form.Label>
                    <Form.Control as="textarea" onChange={handleChange}required rows={3} />
                </Form.Group>

                <Button variant="primary" type="submit" value="Submit" onClick={handleSubmit}>
                    Request
                </Button>

            </Form>
        </Container>
    );
};

export default LeaveRequestForm;
