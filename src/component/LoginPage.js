import React, { useState,useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

import "./LoginPage.css";
import PropTypes from 'prop-types';
import { Route , withRouter,Redirect} from 'react-router-dom';
import {Router} from 'react-router';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/fontawesome-free-solid';
import { Alert } from 'react-bootstrap';


 

  function Login({setToken}) {
  // const history = useHistory()
  useEffect(() => {
    if(localStorage.getItem('userInfo')){
      window.location.href = "/leave"
   }else{
      return false
    }
  }, []);

  const onSignUp = () =>{
    window.location.href = "/signup"
  }
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [redirect,setRedirect] = useState(false);
  const [loginmessage,setLoginmessage] = useState(false);
  const [errormessage,setErrormessage] = useState(false);
  
  let history = useHistory();
  const handleSubmit =  async (e) => {
    e.preventDefault();
    console.log(email,password)
    const item = {email,password}

    let result = await fetch('/user/login',{
      method:"POST",
      headers: {
        'Content-Type': 'application/json',
         'Accept':'application/json'
        },
        body: JSON.stringify(item)
    })
    result = await result.json()
    
    if(result && result.error){
      setErrormessage(true)
      setLoginmessage(false)
      return false

    }else{
      setLoginmessage(true)
      setErrormessage(false)
      console.log(result)
      localStorage.setItem("userInfo",JSON.stringify(result && result.token))
      setTimeout(function(){ window.location.href = "/leave" }, 1000);
      // window.location.href = "/leave"

    }
  }

  // if(redirect){
  //   return <Redirect to = "/request-form"></Redirect>
  // }
  

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }


  
  // function handleSubmit(event) {
  //   event.preventDefault();
  // }
  const checkToken = () => {
    if(!localStorage.getItem('userInfo')){
      return  <div style={{background:"yellow",backgroundImage: "linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%)",height:"850px"}}>
         {loginmessage? <Alert variant="success" style={{marginTop:"10px"}}>
                User is sucessfull Login!!!!
                </Alert>:null}
                {errormessage? <Alert variant="danger" style={{marginTop:"10px"}}>
                    Your account isn't activated yet. Please Connect your admin

                </Alert>:null}
       
        <div style={{padding:"150px"}}>
      
      <Card style={{ width: '40rem',margin:"auto",height:"30rem",boxShadow: "0 0 6px #ccc",borderRadius:"5px",paddingTop:"30px"}}>
      {/* <h1> Login</h1> */}
      <h1 style = {{marginLeft: "30px"}}>Leave Management System</h1>
      <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
      <Form style={{width: "35%"}}  onSubmit={handleSubmit}>
        <Form.Group size="xl" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button block size="lg" type="submit" disabled={!validateForm()}>
        <FontAwesomeIcon icon={faUser} />{" "}
          Login
        </Button>
        <Button block size="lg" onClick={onSignUp}>
          SignUp
        </Button>

      </Form>
      <div>
      <img src="http://hr.propstory.com/assets/images/logo_simple.png"/>
      
      
      </div>
      </div>
      </Card>
    </div>
   
    </div>
   
    }else{
      window.location.href="/leave"
    }
  
  }
 

  

  return (
    checkToken()
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}
const HomeWitRouter = withRouter(Login);
export default (Login)
