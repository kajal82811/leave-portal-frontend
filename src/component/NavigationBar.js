import React from 'react';
import { Nav, Navbar,Button,DropdownButton,Dropdown } from 'react-bootstrap';
import styled from 'styled-components';


const Styles = styled.div`
   .navbar {
       background-color: #4aa35f;
   }
   .navbar-brand, .navbar-nav .nav-link {
       color: #ffffff;
    
    &:hover {
        color: #red;
    }
   }.button{
    background-color: #f1813a;
    border-color:none !important;
    box-shadow: none;
 
}.btn-warning {
    
    
    border-color:#f1813a ;
    color: white;
}
   }
   .btn-primary {
    color: #fff;
    background-color: #4aa35f; 
    border-color: #4aa35f; 
}.show>.btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #4aa35f; 
    border-color: #4aa35f; 
}
`;
const onLogout = ()=>{
    localStorage.clear()
    
    
}
export const NavigationBar = () => (
    <Styles>
        <Navbar expand="lg">
            <Navbar.Brand href="/">PropStory</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className=" ml-auto" >
                    <Nav.Item><Nav.Link style ={{marginTop:"6px"}} href="/leave">Home</Nav.Link></Nav.Item>
                    
                    {/* <Nav.Item><Nav.Link  ><DropdownButton id="dropdown-item-button" title={"Requests"}>
                             <Dropdown.Item as="button">Counters</Dropdown.Item>
                            <Dropdown.Item as="button">List of leave Requests</Dropdown.Item>
                            <Dropdown.Item as="button">Request a Leave</Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Item as="button">List of OT Worked</Dropdown.Item>
                            <Dropdown.Item as="button">Submit an OT Request</Dropdown.Item>
                        
                        
                        </DropdownButton></Nav.Link></Nav.Item>
                        <Nav.Item><Nav.Link  ><DropdownButton id="dropdown-item-button" title={"Calenders"}>
                             <Dropdown.Item as="button">My Calender</Dropdown.Item>
                            <Dropdown.Item as="button">Yearly Calender</Dropdown.Item>
                            <Dropdown.Item as="button">My Workmates</Dropdown.Item>
                            <Dropdown.Item as="button">Department</Dropdown.Item>
                            <Dropdown.Item as="button">Global</Dropdown.Item>
                            <Dropdown.Item as="button">Tabular</Dropdown.Item>
                         */}
                        
                        {/* </DropdownButton></Nav.Link></Nav.Item> */}
                    <Nav.Item><Nav.Link href="/request-form"><Button className="button" variant="warning">New Request</Button></Nav.Link></Nav.Item>
                    
                    {/* <Nav.Item><Nav.Link href="/about">Calenders</Nav.Link></Nav.Item> */}
                    <Nav.Item><Nav.Link href="/"><Button onClick={onLogout} className="button" variant="warning">Logout</Button></Nav.Link></Nav.Item>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    </Styles>
)

