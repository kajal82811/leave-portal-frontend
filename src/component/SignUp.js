import React, { Component } from 'react';
import { Form, Button, Col,InputGroup, Container,Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios'
import FlashMessage from 'react-flash-message';
import { Alert } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import './Signup.css';
import Dropdown from 'react-dropdown';

const options = [
    'Advertisment', 'Technical', 'Sales'
  ];
  let SelectedDepartment;
class SignUpForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: "",
            email: "",
            password: "",
            company: "",
            phone: "",
            role: "",
            showMessage:false,
            registermessage:false,
            fillDetailmessage:false,
            validated:false,
            joiningDate:new Date(),
            DOB:new Date(),
            Department:""
        }

        this.changeName = this.changeName.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.changeCompany = this.changeCompany.bind(this);
        this.changePhone = this.changePhone.bind(this);
        this.changeRole = this.changeRole.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

     handleSelect = (option) =>{
        SelectedDepartment = option.value;
       console.log(SelectedDepartment)
       this.setState({Department:SelectedDepartment})
       console.log(SelectedDepartment)
       // setLeavetype(Selectedleave)
     
   }

    changeName(event) {
        this.setState({
            name: event.target.value
        })
    }

    changeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }
    changePassword(event) {
        this.setState({
            password: event.target.value
        })
    }
    changeCompany(event) {
        this.setState({
            company: event.target.value
        })
    }
    changePhone(event) {
        this.setState({
            phone: event.target.value
        })
    }
    changeRole(event) {
        this.setState({
            role: event.target.value
        })
    }
       onLog = () =>{
           this.props.history.push("/")
        
    }
    onSubmit(event) {
        event.preventDefault();
        const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.setState({
        validated:true
    })

        const registered = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            company: this.state.company,
            phone: this.state.phone,
            role: 'TEAM-MEMBER',
            dob:this.state.DOB,
            doj:this.state.joiningDate,
            department:this.state.Department
        
        }
      
        axios.post('/user/register', registered)
            .then(response => {
               
                console.log("Successfully signed in", response.data)
                console.log("Successfully signed in", response.error)
               if(response) {
                   console.log(response)
                   this.setState({
                    registermessage:true,
                    showMessage:false,
                    fillDetailmessage:false
                   })

               }
            
     
        }).catch(error=>{
            console.log("error",error)
            debugger
            if (error.response.data.errors[0].msg=="Please fill all the fields") {
                debugger
                this.setState({
                    fillDetailmessage:true,
                    registermessage:false,
                    showMessage:false,
                   })
                
               
                
              }
          
            else if(error.response.data.errors[0].msg=="Email is already registered"){
                console.log("error",error)
                console.log(error.response.data.errors[0].msg)
                
                this.setState({
                    showMessage:true,
                    registermessage:false,
                    fillDetailmessage:false,


                }) 
               
            }
            })

        // window.location = "/request-form"
    }

   

    render() {
        console.log(this.state.DOB)
        return (
           
            <div>
                
                   {this.state.showMessage? <Alert variant="success" style={{marginTop:"10px"}}>
                User is already Registered!!!!
                </Alert>:null}
                {this.state.registermessage? <Alert variant="danger" style={{marginTop:"10px"}}>
                User is Registered Successfully!!!!
                </Alert>:null}
                {this.state.fillDetailmessage? <Alert variant="danger" style={{marginTop:"10px"}}>
                Please fill all the fields!!!!!!
                </Alert>:null}
                <div style={{background:"yellow",backgroundImage: "linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%)",height:"850px"}}>
                <Card style={{ width: '50rem',margin:"auto",height:"53rem",boxShadow: "0 0 6px #ccc",borderRadius:"5px"}}>
                {/* <h1 style={{textAlign:"center"}}>SignUp</h1> */}
                <img style={{display: "block",
  marginLeft: "auto",
  marginRight: "auto",
  width: "20%"}}src="http://hr.propstory.com/assets/images/logo_simple.png"/>
                {/* {this.state.showMessage? <Alert variant="danger" style={{marginTop:"10px"}}>
                User is already Registered!!!!
                </Alert>:null} */}
                {/* {this.state.registermessage? <Alert variant="danger" style={{marginTop:"10px"}}>
                User is Registered Successfully!!!!
                </Alert>:null} */}
                <div className="container" style={{marginTop:"-60px"}}>
               
                <Form noValidate validated={this.state.validated} onSubmit={this.onSubmit}>
    
                
            <Form.Group controlId="formBasicName validationCustomUsername">
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="name"
                                placeholder="Enter your Full Name"
                                onChange={this.changeName}
                                value={this.state.name}
                                required
                            />

                        </Form.Group>
                        <Form.Group controlId="formBasicEmail validationCustom02">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" 
                            required
                            placeholder="Enter email" onChange={this.changeEmail}
                                value={this.state.email} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                        </Form.Text>
                        </Form.Group>
        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={this.changePassword}
                            required
                                value={this.state.password} />
                        </Form.Group>
      
      <Form.Group controlId="formBasicCompany">
                            <Form.Label>Company</Form.Label>
                            <Form.Control type="text" placeholder="Company Name" 
                            required
                            onChange={this.changeCompany}
                                value={this.state.company} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPhone">
                            <Form.Label>Phone</Form.Label>
                            <Form.Control type="number" 
                            required
                            placeholder="Phone number" onChange={this.changePhone}
                                value={this.state.phone} />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Date Of Birth(DOB)</Form.Label>
                            <DatePicker selected={this.state.DOB} onChange={date => this.setState({DOB: date})} className="dob"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicPhone">
                            <Form.Label>Date Of Joining(DOJ)</Form.Label>
                            <DatePicker
                                    selected={this.state.joiningDate} 
                                   onChange={date => this.setState({joiningDate: date})} 
                                   className="dob"
                                    
                                />
                        </Form.Group>
                        <Form.Group controlId="formBasicCompany">
                            <Form.Label>Department</Form.Label>
                            <Dropdown options={options} value={this.state.Department} onChange={this.handleSelect}  />
                            
                        </Form.Group>
                        <Button variant="primary" type="submit" value="submit"  >
                            Register
                        </Button>{"     "}
                        <Button variant="primary" type="submit" value="submit" onClick={this.onLog}>
                            Login
                        </Button>
     
    </Form>
  
                </div>
                </Card>
                </div>
            </div>
           
        )
    }
}

export default SignUpForm
