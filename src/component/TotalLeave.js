import React, { Component } from 'react';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Nav, Navbar,Container } from 'react-bootstrap';
import { Form, Button, Col } from 'react-bootstrap';
import styled from 'styled-components';
import axios from 'axios';
// import moment from 'moment';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { Link } from 'react-router-dom';
import LeaveTable from './LeaveTable';
import lodash from 'lodash'
var moment = require('moment-business-days');
class TotalLeave extends Component {
    constructor(props){
        super(props)
          this.state ={
            result:[],
            totalleave2021:"",
            totalleave2022:""
    
    
        }
      }
      componentDidMount(){



        // console.log("in detail",id)
      axios.post(`/leave/list`,{
            id: "602e139d5c3ead4657912075",
      }
        )
      .then(res => {
        console.log(res);
        console.log(res.data.user)
    //     const listItems = res.data.user.map.map((item,key) =>
    // // Wrong! The key should have been specified here:
    //       console.log("item",item._id)
    //  );
       
        this.setState({result:res.data.user})
        console.log(this.state.result)
        var ll = this.state.result.map((item)=>{ return moment(item.start_date).format("YYYY/MM/DD")})
        console.log("ll",ll)
        //  var m = moment(ll.map()).format("YYYY/MM/DD")
        //  console.log(m)
         var duration2021 = []
var duration2022 = []
this.state.result.map(element =>{ if(element && element.duration && moment(moment(element.start_date).format("YYYY/MM/DD")).isBetween("2020/03/31", "2021/04/01")){
    duration2021.push(element.duration)
   }})
   this.state.result.map(element=>{if(element && element.duration && moment(moment(element.start_date).format("YYYY/MM/DD")).isBetween("2021/03/31", "2022/04/01")){duration2022.push(element.duration)}})

console.log(duration2021)
console.log(duration2022)

   const duration2021innumber = duration2021.map((i) => Number(i));
console.log(duration2021innumber)

const durationNumber2021 = duration2021innumber
    .reduce((a, b) => a + b, 0)
  
    this.setState({totalleave2021:durationNumber2021})
 
console.log(duration2021)
console.log(duration2022)

const duration2022innumber = duration2022.map((i) => Number(i));
const durationNumber2022 = duration2022innumber
.reduce((a, b) => a + b, 0)
this.setState({totalleave2022:durationNumber2022})
      })
     




    }
    render() {
        return (
            <div>
               
               <h1>My Summary</h1>
               <h6>Total Leave taken by user in 2020-2021:{this.state.totalleave2021}</h6>
               <h6>Total Leave taken by user in 2021-2022:{this.state.totalleave2022}</h6>
                
            </div>
        );
    }
}

export default TotalLeave;

